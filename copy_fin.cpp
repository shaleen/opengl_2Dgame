#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>


using namespace std;

struct VAO {
    GLuint VertexArrayID;
    GLuint VertexBuffer;
    GLuint ColorBuffer;

    GLenum PrimitiveMode;
    GLenum FillMode;
    int NumVertices;
};
typedef struct VAO VAO;

struct g{
    double x;
    double y;
    double vx;
    double vy;
};
typedef struct g gen;

struct C{
    VAO *circle;
    float radius;
    gen center;
    float mass;
    bool movable;
    bool used; 
};
typedef struct C Circle;

struct R{
    VAO *rectangle;
    gen center;
    float mass;
    float length;
    float breadth;
    bool movable;
    int angle;
    int rotation_stat;
};
typedef struct R Rectangle;

int init_cal =0;  //First time calculate the vx and vy
bool Fire =0; //Fire the cannon
float Ammo_speed =0;
Rectangle CanonR;
Rectangle Speedbar;
Circle CanonC;
Circle Ammo;
Rectangle Ground;
Rectangle Obs[10];
Circle Time;
float current_time;
float last_time;
float air_resistance = 0.99;
int flag=0, a=0;
int Obs_x, Obs_y, Obs_r,np;
double final_x, final_y;
double pan=0, zoom=1;
int Score =0;
double l_time, curr_time;  //These the time stamps of the glfwgetime()
int interval;

struct GLMatrices {
    glm::mat4 projection;
    glm::mat4 model;
    glm::mat4 view;
    GLuint MatrixID;
} Matrices;
GLuint programID;
/* Function to load Shaders - Use it as it is */
GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path) {

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open())
    {
        std::string Line = "";
        while(getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::string Line = "";
        while(getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> VertexShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);

    // Link the program
    fprintf(stdout, "Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> ProgramErrorMessage( max(InfoLogLength, int(1)) );
    glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}

static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

void quit(GLFWwindow *window)
{
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}


/* Generate VAO, VBOs and return VAO handle */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat* color_buffer_data, GLenum fill_mode=GL_FILL)
{
    struct VAO* vao = new struct VAO;
    vao->PrimitiveMode = primitive_mode;
    vao->NumVertices = numVertices;
    vao->FillMode = fill_mode;

    // Create Vertex Array Object
    // Should be done after CreateWindow and before any other GL calls
    glGenVertexArrays(1, &(vao->VertexArrayID)); // VAO
    glGenBuffers (1, &(vao->VertexBuffer)); // VBO - vertices
    glGenBuffers (1, &(vao->ColorBuffer));  // VBO - colors

    glBindVertexArray (vao->VertexArrayID); // Bind the VAO 
    glBindBuffer (GL_ARRAY_BUFFER, vao->VertexBuffer); // Bind the VBO vertices 
    glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW); // Copy the vertices into VBO
    glVertexAttribPointer(
            0,                  // attribute 0. Vertices
            3,                  // size (x,y,z)
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
            );

    glBindBuffer (GL_ARRAY_BUFFER, vao->ColorBuffer); // Bind the VBO colors 
    glBufferData (GL_ARRAY_BUFFER, 3*numVertices*sizeof(GLfloat), color_buffer_data, GL_STATIC_DRAW);  // Copy the vertex colors
    glVertexAttribPointer(
            1,                  // attribute 1. Color
            3,                  // size (r,g,b)
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
            );

    return vao;
}

/* Generate VAO, VBOs and return VAO handle - Common Color for all vertices */
struct VAO* create3DObject (GLenum primitive_mode, int numVertices, const GLfloat* vertex_buffer_data, const GLfloat red, const GLfloat green, const GLfloat blue, GLenum fill_mode=GL_FILL)
{
    GLfloat* color_buffer_data = new GLfloat [3*numVertices];
    for (int i=0; i<numVertices; i++) {
        color_buffer_data [3*i] = red;
        color_buffer_data [3*i + 1] = green;
        color_buffer_data [3*i + 2] = blue;
    }

    return create3DObject(primitive_mode, numVertices, vertex_buffer_data, color_buffer_data, fill_mode);
}

/* Render the VBOs handled by VAO */
void draw3DObject (struct VAO* vao)
{
    // Change the Fill Mode for this object
    glPolygonMode (GL_FRONT_AND_BACK, vao->FillMode);

    // Bind the VAO to use
    glBindVertexArray (vao->VertexArrayID);

    // Enable Vertex Attribute 0 - 3d Vertices
    glEnableVertexAttribArray(0);
    // Bind the VBO to use
    glBindBuffer(GL_ARRAY_BUFFER, vao->VertexBuffer);

    // Enable Vertex Attribute 1 - Color
    glEnableVertexAttribArray(1);
    // Bind the VBO to use
    glBindBuffer(GL_ARRAY_BUFFER, vao->ColorBuffer);

    // Draw the geometry !
    glDrawArrays(vao->PrimitiveMode, 0, vao->NumVertices); // Starting from vertex 0; 3 vertices total -> 1 triangle
}

/**************************
 * Customizable functions *
 **************************/

void keyboard (GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // Function is called first on GLFW_PRESS.

    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_A:
                //canon up
                CanonR.rotation_stat = 2;
                break;
            case GLFW_KEY_B:
                //canon down
                CanonR.rotation_stat = -2;
                break;
            case GLFW_KEY_F:
                //Launch faster
                Ammo_speed += 0.2;
                if(Ammo_speed >= 3)
                {
                    Ammo_speed = 3;
                }
                break;
            case GLFW_KEY_S:
                //Launch slower
                Ammo_speed -= 0.2;
                if(Ammo_speed <= 0)
                {
                    Ammo_speed = 0;
                }
                break;
            case GLFW_KEY_SPACE:
                //LAunch
                Fire = 1;
                //cout << Ammo_speed << endl;
                break;
            case GLFW_KEY_R:
                flag =0;
                pan = 0;
                zoom = 1;
                break;
            case GLFW_KEY_LEFT:
                //pan left
                pan += 0.5;
                break;
            case GLFW_KEY_RIGHT:
                //pan right
                pan -= 0.5;
                break;
            case GLFW_KEY_UP:
                //Zoom in
                zoom += 0.2;
                break;
            case GLFW_KEY_DOWN:
                //Zoom out
                zoom -= 0.2;
                break;
            default:
                break;
        }
    }
    else if (action == GLFW_RELEASE) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                quit(window);
                break;
            default:
                break;
        }
    }
    else if (action == GLFW_REPEAT){
        switch (key){
            case GLFW_KEY_A:
                //canon up
                CanonR.rotation_stat = 2;
                break;
            case GLFW_KEY_B:
                //canon down
                CanonR.rotation_stat = -2;
                break;
            case GLFW_KEY_LEFT:
                //pan left
                break;
            case GLFW_KEY_RIGHT:
                //pan right
                break;
            case GLFW_KEY_UP:
                //Zoom in
                break;
            case GLFW_KEY_DOWN:
                //Zoom out
                break;
            default:
                break;
        }
    }
}

/* Executed for character input (like in text boxes) */
void keyboardChar (GLFWwindow* window, unsigned int key)
{
    switch (key) {
        case 'Q':
        case 'q':
            quit(window);
            break;
        default:
            break;
    }
}

/* Executed when a mouse button is pressed/released */
void mouseButton (GLFWwindow* window, int button, int action, int mods)
{
    switch (button) {
        case GLFW_MOUSE_BUTTON_LEFT:
            Fire = 1;
            break;
        case GLFW_MOUSE_BUTTON_RIGHT:
            break;
        default:
            break;
    }
}

/* Executed when window is resized to 'width' and 'height' */
/* Modify the bounds of the screen here in glm::ortho or Field of View in glm::Perspective */
void reshapeWindow (GLFWwindow* window, int width, int height)
{
    int fbwidth=width, fbheight=height;
    /* With Retina display on Mac OS X, GLFW's FramebufferSize
       is different from WindowSize */
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);

    GLfloat fov = 90.0f;

    // sets the viewport of openGL renderer
    glViewport (0, 0, (GLsizei) fbwidth, (GLsizei) fbheight);

    // set the projection matrix as perspective
    /* glMatrixMode (GL_PROJECTION);
       glLoadIdentity ();
       gluPerspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1, 500.0); */
    // Store the projection matrix in a variable for future use
    // Perspective projection for 3D views
    // Matrices.projection = glm::perspective (fov, (GLfloat) fbwidth / (GLfloat) fbheight, 0.1f, 500.0f);

    // Ortho projection for 2D views
    float nx,ny,px,py;
    nx = (zoom *(-30.0f)) + pan;
    ny = (zoom *(-30.0f));
    px = (zoom *(30.0f)) + pan;
    py = (zoom *(30.0f));
    //Matrices.projection = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, 0.1f, 500.0f);
    Matrices.projection = glm::ortho( nx, px, ny, py, 0.1f, 500.0f);
}

double DEG2RAD(int a)
{
    double r;
    r = (a*M_PI)/180;
    return r;
}

void createCanonC() 
{
    int i, k=3;
    CanonC.movable = 0;
    CanonC.radius = 2;
    CanonC.center.x = -28;
    CanonC.center.y = -25;
    CanonC.center.vx = 0;
    CanonC.center.vy = 0;
    CanonC.mass = M_PI * CanonC.radius * CanonC.radius;  //Area as mass

    GLfloat vertex_buffer_data [1083] ={};
    GLfloat color_buffer_data [1083] ={};
    //This is to set the center as (0,0,0) just for ease
    vertex_buffer_data[0] = 0;
    color_buffer_data[0] = 1;
    vertex_buffer_data[1] = 0;
    color_buffer_data[1] = 0;
    vertex_buffer_data[2] = 0;
    color_buffer_data[2] = 0;
    for(i=0;i<=360;i++)
    {
        vertex_buffer_data[k] = CanonC.radius * cos(DEG2RAD(i));
        color_buffer_data[k] = 1;
        k+=1;
        vertex_buffer_data[k] = CanonC.radius * sin(DEG2RAD(i));
        color_buffer_data[k] = 0;
        k+=1;
        vertex_buffer_data[k] =0;
        color_buffer_data[k] = 0;
        k+=1;
    }
    CanonC.circle = create3DObject(GL_TRIANGLE_FAN, 362, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createAmmo()
{
    int  i, k=3;
    Ammo.movable =1;
    Ammo.radius = 1;
    Ammo.center.x = CanonC.center.x;
    Ammo.center.y = CanonC.center.y;
    Ammo.center.vx = 0;
    Ammo.center.vy = 0;
    Ammo.mass = M_PI * Ammo.radius * Ammo.radius;
    GLfloat vertex_buffer_data [1083] = {};
    GLfloat color_buffer_data [1083] = {};

    vertex_buffer_data[0] = 0;
    color_buffer_data[0] = 1;
    vertex_buffer_data[1] = 0;
    color_buffer_data[1] = 0;
    vertex_buffer_data[2] = 0;
    color_buffer_data[2] = 0;
    for(i=0;i<=360;i++)
    {
        vertex_buffer_data[k] = Ammo.radius * cos(DEG2RAD(i));
        color_buffer_data[k] = 1;
        k+=1;
        vertex_buffer_data[k] = Ammo.radius * sin(DEG2RAD(i));
        color_buffer_data[k] = 0;
        k+=1;
        vertex_buffer_data[k] =0;
        color_buffer_data[k] = 0;
        k+=1;
    }
    Ammo.circle = create3DObject(GL_TRIANGLE_FAN, 362, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createCanonR()
{
    CanonR.center.x = 0;    
    CanonR.center.y = 0;    
    CanonR.center.vx = 0;    
    CanonR.center.vx = 0;    
    CanonR.movable = 0;
    CanonR.angle = 1;

    static const GLfloat vertex_buffer_data [] = {
        0,(CanonC.radius/2),0,
        0,-(CanonC.radius/2),0,
        (CanonC.radius+2),(CanonC.radius/2) ,0, 

        0,-(CanonC.radius/2),0, 
        (CanonC.radius+2),-(CanonC.radius /2),0,
        (CanonC.radius+2),(CanonC.radius/2) ,0
    };

    static const GLfloat color_buffer_data [] = {
        1,0,0, 
        1,0,0,
        1,0,0, 

        1,0,0,
        1,0,0,
        1,0,0
    };

    CanonR.rectangle = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createTimeCircle()
{
    int i=0, k=3;
    Time.center.y = 24;
    Time.center.x = -21;
    Time.radius = 4;
    Time.movable =0;
    GLfloat vertex_buffer_data [1083] ={};
    GLfloat color_buffer_data [1083] ={};
    //This is to set the center as (0,0,0) just for ease
    vertex_buffer_data[0] = 0;
    color_buffer_data[0] = ((double) rand() / (RAND_MAX));
    vertex_buffer_data[1] = 0;
    color_buffer_data[1] = ((double) rand() / (RAND_MAX));
    vertex_buffer_data[2] = 0;
    color_buffer_data[2] = ((double) rand() / (RAND_MAX));
    for(i=1;i<((360/(interval+1))*(curr_time-l_time+1)); i++)
    {
        vertex_buffer_data[k] = Time.radius * cos(DEG2RAD(i));
        color_buffer_data[k] = ((double) rand() / (RAND_MAX));
        k+=1;
        vertex_buffer_data[k] = Time.radius * sin(DEG2RAD(i));
        color_buffer_data[k] = ((double) rand() / (RAND_MAX));
        k+=1;
        vertex_buffer_data[k] =0;
        color_buffer_data[k] = ((double) rand() / (RAND_MAX));
        k+=1;
    }
    //Time.circle = create3DObject(GL_LINES, 362, vertex_buffer_data, color_buffer_data, GL_LINE);
    Time.circle = create3DObject(GL_TRIANGLE_FAN, 362, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createGround()
{
    Ground.center.x = 0;
    Ground.center.y = -30;
    Ground.center.vx = 0;
    Ground.center.vy = 0;
    Ground.movable = 0;
    Ground.angle = 0;

    static const GLfloat vertex_buffer_data [] = {
        -30,-4,0, // vertex 1
        30,-4,0, // vertex 2
        30, 4,0, // vertex 3

        30, 4,0, // vertex 3
        -30, 4,0, // vertex 4
        -30,-4,0  // vertex 1
    };

    static const GLfloat color_buffer_data [] = {
        0.34,0.51,0.23, // color 1
        0.34,0.51,0.23, // color 1
        0.34,0.51,0.23, // color 1
        0.34,0.51,0.23, // color 1
        0.34,0.51,0.23, // color 1
        0.34,0.51,0.23, // color 1
    };

    Ground.rectangle = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createSpeedbar()
{
    Speedbar.center.x =0;
    Speedbar.center.y =0;
    Speedbar.center.vx =0;
    Speedbar.center.vy =0;

    GLfloat vertex_buffer_data [] = {
        -1,2,0, // vertex 1
        -1,-2,0, // vertex 2
        (15*Ammo_speed),-2,0, // vertex 3

        -1, 2,0, // vertex 1
        (15*Ammo_speed), 2,0, // vertex 4
        (15*Ammo_speed),-2,0, // vertex 3
    };

    GLfloat color_buffer_data [] = {
        (1-(Ammo_speed/3.0)),0,0, // color 1
        (1-(Ammo_speed/3.0)),0,0, // color 2
        0,(Ammo_speed/3.0),0, // color 3
        (1-(Ammo_speed/3.0)),0,0, // color 1
        0,(Ammo_speed/3.0),0, // color 4
        0,(Ammo_speed/3.0),0 // color 3
    };
    Speedbar.rectangle = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color_buffer_data, GL_FILL);
}

void createObstacle()
{
    int a; 
    for(a=0; a<10; a++)
    {
        int x, y;
        x = rand() % 3 +1;
        y = rand() % 3 +1;
        Obs[a].length = 2*x;
        Obs[a].breadth = 2*y;
        Obs[a].center.x = 0;
        Obs[a].center.y = 0;
        Obs[a].center.vx = 0;
        Obs[a].center.vy = 0;
        GLfloat vertex_buffer_data [] = {
            -x,y,0,
            -x,-y,0,
            x, y,0, 

            -x, -y, 0,
            x, -y, 0,
            x, y, 0
        };
        GLfloat color_buffer_data [] ={
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
            (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX,
        };
        Obs[a].rectangle = create3DObject(GL_TRIANGLES, 6, vertex_buffer_data, color_buffer_data, GL_FILL);
    }
}

void checkCollision()
{
    for (a=0; a<10; a++)
    {
        glm::vec2 center(Ammo.center.x, Ammo.center.y);
        // Calculate AABB info (center, half-extents)
        glm::vec2 aabb_half_extents(Obs[a].length/2, Obs[a].breadth/2);
        glm::vec2 aabb_center(Obs[a].center.x, Obs[a].center.y);

        // Get difference vector between both centers
        glm::vec2 difference = center - aabb_center;
        glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
        // Add clamped value to AABB_center and we get the value of box closest to circle
        glm::vec2 closest = aabb_center + clamped;
        // Retrieve vector between center circle and closest point AABB and check if length <= radius
        difference = closest - center;
        if(glm::length(difference) < Ammo.radius)
        {
            cout << "Collision on " << a <<  endl;
            Ammo.center.x = CanonC.center.x;
            Ammo.center.y = CanonC.center.y;
            init_cal = 0;
            current_time =0;
            Fire = 0;
            Obs[a].center.x =100;
            Obs[a].center.y =100;
            Ammo.center.vx *= -0.6;
            Score += 1;
            cout << "Score:- " << Score << endl;
        }
    }
}

void projectile(int angle)
{
    current_time += 0.1;
    if(init_cal == 0)
    {
        Ammo.center.vy = Ammo_speed * cos(DEG2RAD(90-angle));
        Ammo.center.vx = Ammo_speed * sin(DEG2RAD(90-angle));
        init_cal = 1;
    }
    Ammo.center.x += Ammo.center.vx;
    Ammo.center.y += Ammo.center.vy;
    Ammo.center.vy -= 0.03 * current_time;
    Ammo.center.vx *= 0.998;
    if(Ammo.center.y < -25)
    {
        //Fire =0;
        Ammo.center.vy *= -0.7;
        if(abs(Ammo.center.vy) < 0.2 || current_time >= 15)
        {
            Fire =0;
            Ammo.center.x = CanonC.center.x;
            Ammo.center.y = CanonC.center.y;
            Ammo.center.vy = 0;
            Ammo.center.vx = 0;
            init_cal = 0;
            current_time = 0;
        }
    }
    //check for collision
    // Get center point circle first 
    /*for (a=0; a<10; a++)
      {
      glm::vec2 center(Ammo.center.x, Ammo.center.y);
    // Calculate AABB info (center, half-extents)
    glm::vec2 aabb_half_extents(Obs[a].length/2, Obs[a].breadth/2);
    glm::vec2 aabb_center(Obs[a].center.x, Obs[a].center.y);

    // Get difference vector between both centers
    glm::vec2 difference = center - aabb_center;
    glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
    // Add clamped value to AABB_center and we get the value of box closest to circle
    glm::vec2 closest = aabb_center + clamped;
    // Retrieve vector between center circle and closest point AABB and check if length <= radius
    difference = closest - center;
    if(glm::length(difference) < Ammo.radius)
    {
    cout << "Collision on " << a <<  endl;
    Ammo.center.x = CanonC.center.x;
    Ammo.center.y = CanonC.center.y;
    init_cal = 0;
    current_time =0;
    Fire = 0;
    Obs[a].center.x =100;
    Obs[a].center.y =100;
    Ammo.center.vx *= -0.6;
    }
    }
    */
    checkCollision();
}

float camera_rotation_angle = 90;

void draw ()
{

    createSpeedbar();
    createTimeCircle();
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    glm::vec3 eye ( 5*cos(DEG2RAD(camera_rotation_angle)), 0, 5*sin(DEG2RAD(camera_rotation_angle)));
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target (0, 0, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up (0, 1, 0);

    // Compute Camera matrix (view)
    // Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    //  Don't change unless you are sure!!
    Matrices.view = glm::lookAt(glm::vec3(0,0,3), glm::vec3(0,0,0), glm::vec3(0,1,0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    //  Don't change unless you are sure!!
    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    //  Don't change unless you are sure!!
    glm::mat4 MVP;	// MVP = Projection * View * Model

    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translateGround = glm::translate (glm::vec3(Ground.center.x,Ground.center.y,0)); // glTranslatef
    glm::mat4 rotateGround = glm::rotate((float)(0), glm::vec3(0,0,1));  // rotate about vector (1,0,0)
    glm::mat4 GroundTransform = translateGround ;//* rotateCanonR;
    Matrices.model *= GroundTransform; 
    MVP = VP * Matrices.model; // MVP = p * V * M
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(Ground.rectangle);

    //Text to be rendered 
    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translateSpeedbar = glm::translate (glm::vec3(-24,-29,0)); // glTranslatef
    glm::mat4 SpeedbarTransform = translateSpeedbar;
    Matrices.model *= SpeedbarTransform; 
    MVP = VP * Matrices.model; // MVP = p * V * M
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(Speedbar.rectangle);

    //Ammo
    Matrices.model = glm::mat4(1.0f);
    if(Fire == 1)
    {
        projectile(CanonR.angle);
    }
    glm::mat4 translateAmmo = glm::translate (glm::vec3(Ammo.center.x,Ammo.center.y, 0)); // glTranslatef
    glm::mat4 AmmoTransform = translateAmmo;
    Matrices.model *= AmmoTransform; 
    MVP = VP * Matrices.model; // MVP = p * V * M
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(Ammo.circle);

    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translateCanonC = glm::translate (glm::vec3(CanonC.center.x,CanonC.center.y, 0)); // glTranslatef
    glm::mat4 CanonCTransform = translateCanonC;
    Matrices.model *= CanonCTransform; 
    MVP = VP * Matrices.model; // MVP = p * V * M
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(CanonC.circle);

    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translateTime = glm::translate (glm::vec3(Time.center.x, Time.center.y, 0)); // glTranslatef
    glm::mat4 TimeTransform = translateTime;
    Matrices.model *= TimeTransform; 
    MVP = VP * Matrices.model; // MVP = p * V * M
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(Time.circle);

    Matrices.model = glm::mat4(1.0f);
    glm::mat4 translateCanonR = glm::translate (glm::vec3(CanonC.center.x, CanonC.center.y, 0)); // glTranslatef
    glm::mat4 rotateCanonR = glm::rotate((float)(DEG2RAD(CanonR.angle)), glm::vec3(0,0,1));  // rotate about vector (1,0,0)
    glm::mat4 CanonRTransform = translateCanonR * rotateCanonR;
    Matrices.model *= CanonRTransform; 
    MVP = VP * Matrices.model; // MVP = p * V * M
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(CanonR.rectangle);
    CanonR.angle += CanonR.rotation_stat;
    if (CanonR.angle > 90)
        CanonR.angle = 90;
    else if (CanonR.angle < 0 )
        CanonR.angle = 0;
    CanonR.rotation_stat =0;
    ///OBSTACLE

    for(a=0; a<10;a++) 
    {
        if(flag < 10)
        {
            np = rand()%2;
            if(np == 0)
            {
                Obs_x = -rand()%15;
            }
            else
                Obs_x = rand()%15;

            np = rand()%2;
            if(np == 0)
            {
                Obs_y = -rand()%15;
            }
            else
                Obs_y = rand()%15;

            Obs_r = rand()%180;
            Obs[a].angle = Obs_r;
            Obs[a].center.x = Obs_x;
            Obs[a].center.y = Obs_y;
            flag++;
            //cout << np << " " << Obs_x << " " << Obs_y << " " << Obs_r << endl;
        }
        Matrices.model = glm::mat4(1.0f);
        glm::mat4 translateObs;
        glm::mat4 rotateObs;
        glm::mat4 ObsTransform;
        translateObs = glm::translate (glm::vec3(Obs[a].center.x,Obs[a].center.y,0)); // glTranslatef
        rotateObs = glm::rotate((float)(Obs[a].angle), glm::vec3(0,0,1));  // rotate about vector (1,0,0)
        ObsTransform = translateObs * rotateObs;
        Matrices.model *= ObsTransform;
        MVP = VP * Matrices.model; // MVP = p * V * M
        glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
        draw3DObject(Obs[a].rectangle);
    }


}

void CursorPosition(GLFWwindow *window, double x_position,double y_position)
{
    //cout << x_position << " " << y_position << endl;
    final_x = x_position/10.0f;
    final_y = y_position/10.0f;
    final_y *= -1;
    final_x -= 30;
    final_y += 30;
    //cout << final_x << " " << final_y << endl;
    glm::vec2 linesegment(final_x, final_y);  
    glm::vec2 linefixed(CanonC.center.x, CanonC.center.y);  
    glm::vec2 difference = linesegment - linefixed;
    glm::vec2 xaxis(1.0,0.0);
    float dotproduct = glm::dot(difference, xaxis);
    float theta = dotproduct/(glm::length(difference)*glm::length(xaxis));
    CanonR.angle = acos (theta) * 180.0 / M_PI;
    //cout << "rotaion angle " << CanonR.angle << endl;
    //Ammmo_speed
    double length;
    length = sqrt(abs(pow((final_x - CanonC.center.x), 2) + pow((final_y - CanonC.center.y), 2)));
    //cout << "length " << length << endl;
    Ammo_speed = length / 10;
}

void Scroll(GLFWwindow *window, double x, double y)
{
    if (y < 0)
    {
        zoom -= 0.2;
    }
    else if (y > 0)
    {
        zoom += 0.2;
    }
}

GLFWwindow* initGLFW (int width, int height)
{
    GLFWwindow* window; // window desciptor/handle

    glfwSetErrorCallback(error_callback);
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "Sample OpenGL 3.3 Application", NULL, NULL);

    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval( 1 );

    /* --- register callbacks with GLFW --- */

    /* Register function to handle window resizes */
    /* With Retina display on Mac OS X GLFW's FramebufferSize
       is different from WindowSize */
    glfwSetFramebufferSizeCallback(window, reshapeWindow);
    glfwSetWindowSizeCallback(window, reshapeWindow);

    /* Register function to handle window close */
    glfwSetWindowCloseCallback(window, quit);

    /* Register function to handle keyboard input */
    glfwSetKeyCallback(window, keyboard);      // general keyboard input
    glfwSetCharCallback(window, keyboardChar);  // simpler specific character handling

    /* Register function to handle mouse click */
    glfwSetMouseButtonCallback(window, mouseButton);  // mouse button clicks

    glfwSetCursorPosCallback(window, CursorPosition);

    glfwSetScrollCallback(window,Scroll);
    return window;
}

/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL (GLFWwindow* window, int width, int height)
{
    /* Objects should be created before any other gl function and shaders */
    // Create the models
    createCanonC();
    createCanonR();
    createGround();
    createAmmo();
    createObstacle();
    createSpeedbar();
    //createTimeCircle();
    // Create and compile our GLSL program from the shaders
    programID = LoadShaders( "Sample_GL.vert", "Sample_GL.frag" );
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");


    reshapeWindow (window, width, height);

    // Background color of the scene
    glClearColor (0.28f, 0.38f, 0.63f, 0.0f); // R, G, B, A
    glClearDepth (1.0f);

    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);

    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}

int main (int argc, char** argv)
{
    int width = 600;
    int height = 600;

    srand(time(NULL));
    GLFWwindow* window = initGLFW(width, height);

    initGL (window, width, height);

    l_time = glfwGetTime();

    interval = 3;
    /* Draw in loop */
    while (!glfwWindowShouldClose(window)) {

        reshapeWindow (window, width, height);
        curr_time = glfwGetTime();
        //cout << curr_time << " " << last_update_time <<  endl;
        if((curr_time-l_time) > interval)
        {
            flag = 0;
            l_time = curr_time;
        }
        draw();
        // OpenGL Draw commands

        // Swap Frame Buffer in double buffering
        glfwSwapBuffers(window);

        // Poll for Keyboard and mouse events
        glfwPollEvents();

    }
    glfwTerminate();
    exit(EXIT_SUCCESS);
}
