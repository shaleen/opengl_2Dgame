This Is my version of AngryBirds.
Developer: Shaleen Garg
Roll no: 201401069

It is an original state of the art game where the player has to collide with all the 10 obstacles in 5 seconds after which the game resets.

In between the game if the player looses his shot, he will have to pay a penalty of 2 secs.

The main file is copy_fin.cpp

This game can be compiled and run by typing $make;./a.out

Controls:

Keyboard:
Space = fire;
f = faster launch
s = slower launch
a = Canon Move up
b = canon Move down
r = reload
q = quit
Arrow up  = Zoom in
Arrow down = Zoom out
Arrow left = pan left
Arrow right = pan right

Mouse:
Scroll up = Zoom in
Scroll down = zoom out 
left click = fire
Mouse pointer can be moved to change the speed and the angle of the shot


The score will be shown on the terminal as the needed libraries were unable to run in the developers system. (Arch linux)

The programming has been done using vim editor. It is advisable to use vim for all programming because of its agility, versatility and awesomeness.


